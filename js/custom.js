jQuery(document).ready(function () {

    //Wow
    wow = new WOW({

        mobile: false // default
    })
    wow.init();

    // Slider section
    jQuery('.sliding-sec').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
    });

    // Tooltip
    jQuery(function () {
        jQuery('[data-toggle="tooltip"]').tooltip()
    });

    // Notify Icon        
    jQuery(document).on( "click", ".notify-icon a", function() {
        jQuery(this).toggleClass('show');
    });

    // Accordian   
    
    jQuery('.card').click(function(){ 
        if ( jQuery(this).hasClass('bg-color') ) {
           jQuery(this).removeClass('bg-color');
        } else {
           jQuery('.card').removeClass('bg-color');
           jQuery(this).addClass('bg-color');    
        }   
    });
    
    // Profile Upload
    
    jQuery(".user-profile input:file").on('change', function (){
        var fileName = jQuery(this).val();
    });
    
    //file input preview
    function readURL(input) { 
        if (input.files && input.files[0]) {
        var reader = new FileReader();
            reader.onload = function (e) {
                jQuery('.user-profile figure').attr('style', 'background-image: url('+e.target.result   +')');  
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    jQuery('body').find(".user-profile input[type='file']").on('change',function(){
        readURL(this);
    });

});

// Custom Scrollbar 

(function(jQuery){
    jQuery(window).on("load",function(){
        jQuery(".notificationContent").mCustomScrollbar();
    });
})(jQuery);


// Upload Page File Add
 jQuery(".add_files input:file").on('change', function (){
    var fileName = jQuery(this).val();
});

function readURL(input) { 
    console.log(input.files["0"].name);
    if (input.files && input.files[0]) {
    var reader = new FileReader();
        reader.onload = function (e) {
            jQuery('.file-upload-dec figure').attr('style', 'background-image: url('+e.target.result   +')');  
        }
        reader.readAsDataURL(input.files[0]);
         jQuery("#uploadFile").val(input.files["0"].name);
    }
}
jQuery('body').find(".add_files input[type='file']").on('change',function(){
    readURL(this);
});

//Header

jQuery(window).on("load resize scroll", function(e) {
  var Win = jQuery(window).height();
  var Header = jQuery("header").height();
  var Footer = jQuery("footer").height();

  var NHF = Header + Footer;

  jQuery('.main').css('min-height', (Win - NHF));

});

// Testimonial Slider
    
jQuery('.testimonial-slider').owlCarousel({
    loop:false,
    margin:150,
    nav:false,
    autoPlay : true,
    dots:true,
    navText : false,
    responsive:{
        0:{
            items:1
        },
       600:{
            items:2,
            margin: 0
        },
        1000:{
            items:3,
            margin: 50
        }
    }
});

//Page Zoom

document.documentElement.addEventListener('touchstart', function (event) {
 if (event.touches.length > 1) {
   event.preventDefault();
 }
}, false);

// Header Scroll

jQuery(document).ready(function () {
    jQuery(document).on("scroll", onScroll);
    //smoothscroll
    jQuery('.navbar-nav li a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        jQuery(document).off("scroll");
        jQuery('.navbar-nav li a').each(function () {
            jQuery(this).removeClass('active');
        })
        jQuery(this).addClass('active');
        var target = this.hash
            , menu = target;
        target = jQuery(target);
        jQuery('html, body').stop().animate({
            'scrollTop': target.offset().top + 2
        }, 1000, 'swing', function () {
            window.location.hash = target;
            jQuery(document).on("scroll", onScroll);
        });
    });
});

//Model
jQuery(function() {
  //----- OPEN
  jQuery('[data-popup-open]').on('click', function(e) {
    var targeted_popup_class = jQuery(this).attr('data-popup-open');
    jQuery('[data-popup="' + targeted_popup_class + '"]').fadeIn(100);

    e.preventDefault();
  });

  //----- CLOSE
  jQuery('[data-popup-close]').on('click', function(e) {
    var targeted_popup_class = jQuery(this).attr('data-popup-close');
    jQuery('[data-popup="' + targeted_popup_class + '"]').fadeOut(200);

    e.preventDefault();
  });
});

function onScroll(event) {
    var scrollPos = jQuery(document).scrollTop();
    jQuery('.navbar-nav li a').each(function () {
        var currLink = jQuery(this);
        var refElement = jQuery(currLink.attr("href"));
        console.log(refElement);
        if (jQuery(refElement).length > 0 ) { 
            if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
                jQuery('.navbar-nav li a').removeClass("active");
                currLink.addClass("active");
            }
            else {
                currLink.removeClass("active");
            }
        }
    });
}

// Accordian Flex Slider 

(function(){
    jQuery('.flex-container').waitForImages(function() {
        jQuery('.spinner').fadeOut();
}, jQuery.noop, true);

jQuery(".flex-slide").each(function(){
    jQuery(this).hover(function(){
        jQuery(this).find('.flex-title').css({
            transform: 'rotate(0deg)'
        });
        jQuery(this).find('.flex-about').css({
            opacity: '1'
        });
    }, function(){
        jQuery(this).find('.flex-title').css({
            transform: 'rotate(90deg)'
        });
        jQuery(this).find('.flex-about').css({
            opacity: '0'
        });
    })
});
})();


var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-36251023-1']);
_gaq.push(['_setDomainName', 'jqueryscript.net']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

//Textarea 

jQuery(function (jQuery) {
    jQuery('.firstCap, textarea').on('keypress', function (event) {
        var jQuerythis = jQuery(this),
            thisVal = jQuerythis.val(),
            FLC = thisVal.slice(0, 1).toUpperCase();
        con = thisVal.slice(1, thisVal.length);
        jQuery(this).val(FLC + con);
    });
});


//Input Field
jQuery(document).ready(function() {
    var formFields = jQuery('.form-group');

    formFields.each(function() {
        var field = jQuery(this);
        var input = field.find('.form-control');
        var label = field.find('label');

        function checkInput() {
            var valueLength = input.val().length;
            if (valueLength > 0 ) {
                label.addClass('freeze')
            } 
            else {
                label.removeClass('freeze')
            }
        }
        input.change(function() {
            checkInput()
        })
    });
});

jQuery(document).on('click', '.input-file-trigger', function(){
    
// File Upload
document.querySelector("html").classList.add('js');

var fileInput  = document.querySelector( ".input-file" ),  
    button     = document.querySelector( ".input-file-trigger" ),
    the_return = document.querySelector(".file-return");
      
button.addEventListener( "keydown", function( event ) {  
    if ( event.keyCode == 13 || event.keyCode == 32 ) {  
        fileInput.focus();  
    }  
});
button.addEventListener( "click", function( event ) {
   fileInput.focus();
   return false;
});  
fileInput.addEventListener( "change", function( event ) {  
    the_return.innerHTML = this.value;  
});  
        
});